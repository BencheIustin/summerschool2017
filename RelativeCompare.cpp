#define TEST_RELATIVE_EQUALITY 0

bool values_equal( const double dfLeft , const double dfRite)
{
#if TEST_RELATIVE_EQUALITY
  if ( dfLeft == dfRite ) {
    // if values are both zero, relative comparison below will fail
    return true;
  } else {
    const auto dfAbsDiff = std::abs( dfLeft - dfRite );
    const auto dfAvg = ( std::abs( dfLeft ) + std::abs( dfRite ) ) / 2.0;
    return dfAbsDiff < dfAvg * std::numeric_limits< double >::epsilon();
  }
#else
  return dfLeft == dfRite;
#endif
}

int main( void )
{
  if ( values_equal( 123456789.12345678 , 123456789.12345679 ) )
    std::cout << "values are equal " << std::endl;
  else
    std::cout << "values are not equal " << std::endl;
  return 0;
}

