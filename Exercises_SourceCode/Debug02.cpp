#include <stdio.h>

typedef const char* S;
typedef char Player[ 100 ];

void asgn_position( Player name , S pos )
{
  sprintf_s( name , 100 , "%s %s" , pos , name );
}

int main()
{
  Player player = "Joe Jackson";
  asgn_position( player , "QB" );
  printf( "%s\n" , player );
  return 0;
}