#include <iostream>

typedef unsigned Property;
#define PRIME    0x01
#define NEGATIVE 0x02
#define ZERO     0x04

Property int_check( int k )
{
  Property p = 0;
  if ( k < 0 )
    p |= NEGATIVE;
  else if ( k == 0 )
    p &= ZERO;
  else for ( int i = 2;; i++ ) {
    if ( i * i > k ) {
      p |= PRIME;
      break;
    }
    else if ( k % i == 0 )
      break;
  }
  return p;
}

int main( void )
{
  // TODO: find the error in the function above, or write here code to test it
  return 0;
}