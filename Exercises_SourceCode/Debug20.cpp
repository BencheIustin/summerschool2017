#include <stdio.h>

class ClassA {
	int a;
};

void SomeOtherMethod() {
	throw 20;
}

void SomeMethod()
{
	ClassA *a = new ClassA;
	SomeOtherMethod();
	delete a;
}

void main() {
	
	try {
		SomeMethod();
	}
	catch (int e) {
		printf("An exception occurred.Exception Nr. %d\n", e);
	}

}