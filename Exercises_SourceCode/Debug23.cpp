#include <stdio.h>

#define f(x) x*x+1

void main() {

	int a=2, b=3, y;
	y = f(a + b);
	// should be 26 right?

	printf("%d \n", y);
}