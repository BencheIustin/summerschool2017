#include <iostream>

int main()
{
  const double three = 3.0;
  double x , y , z;
  x = 1 / three;
  y = 4 / three;
  z = 5 / three;
  if ( x + y == z )
    std::cout << "1/3 + 4/3 == 5/3 \n";
  else
    std::cout << "1/3 + 4/3 != 5/3 \n";
  return 0;
}