#include <iostream>

int factorial( int n )
{
  int p = 1;
  int i = n;
  /* compute p = n * (n-1) * (n-2) * ... * 2 */
  while ( i >= 2 );
  {
    i--;
    p = *i;
  }
  return i;
}

int main()
{
  int input;
  std::cout << "Please enter an integer: ";
  std::cin >> input;
  std::cout << factorial( input ) << std::endl;
  return 0;
}