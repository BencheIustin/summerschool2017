#include <stdio.h>

#define Knots
const double standard_rating = 10.0 Knots;
double rating;

void rate_the_ship(double, double, int, double);

int main()
{
  rate_the_ship(64.0, 45.3, 3, standard_rating);
  printf("%g\n", rating);
  return 0;
}

void rate_the_ship(double LWL, double sailarea, int nsails, double rating)
{
  rating *= (3.2 / LWL) * sailarea * .025 * nsails;
  return;
}